import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { MobUserLoginModel } from '../models/MobUserLoginModel.model';
import { LoadingService } from '../services/loading-service';
import { LoginService } from '../services/login/login.service';
import { MessageService } from '../services/message-service/message-service';
import jwt_decode from 'jwt-decode';


 import { ConnectionStatus, NetworkProvider } from '../services/network-connection.service';

@Component({
  selector: 'app-folder',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  public showLogin: boolean = true;
  public showverify: boolean = false;
  public userModel: MobUserLoginModel = new MobUserLoginModel();
  public session_id: any;
  public otp: any;
  public returendUserData: any;
  public mobloginbtn: boolean = false;
  jwtToken: any;
  userType: string;
  showSignup: boolean = false;
  constructor(public loadingScreen: LoadingService, public messageService: MessageService, public http: HttpClient
    ,public networkService:NetworkProvider
    , public menueCtrl: MenuController, public loginservice: LoginService, private router: Router) { }

  // disable side menu 
  ionViewWillEnter() {
    this.menueCtrl.enable(false);
  }
  // this validation use  for login with Phone number and Pass.
  loginValidation() {
    if (this.userModel.IsValidCheck(undefined, undefined) == false) {
      for (var i in this.userModel.UserValidator.controls) {
        this.userModel.UserValidator.controls[i].markAsDirty();
        this.userModel.UserValidator.controls[i].updateValueAndValidity();
      }
    }
    else {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
      if (status != ConnectionStatus.Online) {
      this.messageService.waringMessage("Internet connection not available, please check it");       
      }
      else{
      this.loginMobnoandPass();         //Login with Phone number and Password
      }
      });
    }
  }
  //if mob number is falied then login btn enable 
  enableloginbtn() {
    let Use;
    let Used = this.userModel.MobileNo
    if (Use.toFixed[10]) {
      this.mobloginbtn = true;
    }
    else {
      this.mobloginbtn = false;
    }
  }
  //---START------login with mob no pass 

  loginMobnoandPass() {
    this.loginservice.loginMobnoandPass(this.userModel)
      .subscribe(res => this.SuccessloginMobnoandPass(res),
        res => this.loginMobnoandPassFailedError(res));
  }
  SuccessloginMobnoandPass(res) {
    this.userModel = new MobUserLoginModel();
    // login successful if there's a jwt token in the response
    if (res && res.token) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      sessionStorage.removeItem('jwtToken');
      //localStorage.setItem('currentUser', JSON.stringify(user));
      sessionStorage.setItem('jwtToken', res.token);
      localStorage.setItem('login', "true");
      let token = res.token;
      // var decoded = JSON.parse(token);  
      var decodedjwt = jwt_decode(token);
      this.jwtToken = decodedjwt;
      this.userType = decodedjwt.UserType;
      if (this.userType == "Patient") {
        this.router.navigate(['home']);
        //this.currentUserSubject.next(user);
      }
      else{
        this.messageService.errorMessage('Not valid user');
      }
      
    }

    this.mobloginbtn = false;
  }
  loginMobnoandPassFailedError(res) {
    this.messageService.errorMessage('Phone Number and Password do not match');
    this.mobloginbtn = false;
  }
  //---END------login with mob no pass 


  //--------START------login with OTP 
  // Step 1 - post mob no 
 postMobno() {
   this.showverify =true;
   this.showSignup = false;
   this.showLogin= false;
   //---- start temp cmt for UI testing ------------------>>>>>>
    // this.loginservice.postMobno(this.userModel)
    //   .subscribe(res => this.SuccesspostMobno(res),
    //     res => this.postMobnoFailedError(res));
     //---- end temp cmt forui testing ------------------->>>>>>
  }
  SuccesspostMobno(res) { 
    this.userModel = new MobUserLoginModel();
    var data = res.Value;
    this.returendUserData = data;
 //  this.GenerateOTP(data);            // Step - 2
   this.LoginViaOTP();       //temp added code
    this.messageService.successMessage('Wait For OTP... ');
    this.mobloginbtn = false;
   
  }
  postMobnoFailedError(res) {
    this.messageService.ShowCatchErrMessage('Phone Number and Password do not match');
    this.mobloginbtn = false;
  }
  // Step 2 - generate OTP.
  GenerateOTP(userData) {
    try {
      if (userData != null) {
        this.loginservice.GenerateOTP(userData)
          .subscribe(res => this.SuccessOTPGenerate(res),
            res => this.OTPGenerateFailedError(res));
      }
    }
    catch (exception) {
      this.loadingScreen.hideLoader();
      this.messageService.ShowCatchErrMessage(exception);
    }
  }
  SuccessOTPGenerate(res) {
    this.session_id = res.Details;
    this.showLogin = false;
    this.showverify = true;
    this.messageService.successMessage('OTP Genarted');
  }
  OTPGenerateFailedError(res) {
    this.messageService.ShowCatchErrMessage('Server is not available , Please try after sometime');
  }

  // Step 3 - after generating OTP.. need to verify OTP.
  verifyOTP() {
    this.router.navigate(['home']); //temp added code
    try {
      if (this.session_id != null && this.otp != null) {
        this.loginservice.verifyOTP(this.session_id, this.otp)
          .subscribe(res => this.SuccessVerifyOTP(res),
            res => this.VerifyOTPFailedError(res));
      }
    }
    catch (exception) {
      this.loadingScreen.hideLoader();
      this.messageService.ShowCatchErrMessage(exception);
    }
  }
  SuccessVerifyOTP(res) {
    this.router.navigate(['home']);
    this.showLogin = true;
    this.showverify = false;
    this.session_id = null;
    this.otp = null;
    localStorage.setItem('USER_INFO', JSON.stringify(this.returendUserData));
    this.messageService.waringMessage("OTP matched")
    this.LoginViaOTP();                        //Step - 4

  }
  VerifyOTPFailedError(res) {
    this.messageService.waringMessage("OTP not matched")
  }
  // Step 3 - after generating OTP.. need to verify OTP.
  // verifyOTP2() {
  //   //793b129c-03a7-11eb-9fa5-0200cd936042   <== API_KEY

  //   this.http.get<any>("https://2factor.in/API/V1/793b129c-03a7-11eb-9fa5-0200cd936042/SMS/VERIFY/" + this.session_id + "/" + this.otp)
  //     .subscribe(res => {
  //       if (res.Status == "Success") {
  //         this.router.navigate(['home']);
  //         this.showLogin = true;
  //         this.showverify = false;
  //         this.session_id = null;
  //         this.otp = null;
  //         localStorage.setItem('USER_INFO', JSON.stringify(this.returendUserData));     

  //       }
  //       else if (res.Status == "Error") {
  //         this.messageService.waringMessage(res.Details)
  //       }
  //       else {
  //         this.messageService.waringMessage("OTP not matched")
  //       }

  //     },
  //       err => {
  //         this.messageService.waringMessage("OTP not matched")
  //       })
  // }

  // Step - 4  // after varify call telmed login api and get usr data and JWT token 
  LoginViaOTP() {
    try {
      if (this.userModel != null) {
        this.loginservice.LoginViaOTP(this.userModel)
          .subscribe(res => this.SuccessLoginViaOTP(res),
            res => this.LoginViaOTPFailedError(res));
      }
    }
    catch (exception) {
      this.loadingScreen.hideLoader();
      this.messageService.ShowCatchErrMessage(exception);
    }
  }
  SuccessLoginViaOTP(user) {
    this.userModel = new MobUserLoginModel();
    this.router.navigate(['home']); //temp added code
    // login successful if there's a jwt token in the response
    if (user && user.token) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      sessionStorage.removeItem('jwtToken');
      //localStorage.setItem('currentUser', JSON.stringify(user));
      sessionStorage.setItem('jwtToken', user.token);
      localStorage.setItem('login', "true");
      let token = user.token;
      var decoded = jwt_decode(token);
      this.jwtToken = decoded;
      this.userType = decoded.UserType;
    }

  }
  LoginViaOTPFailedError(res) {
    this.messageService.waringMessage("OTP not matched")
  }

  ShowSignUp(){
    this.userModel = new MobUserLoginModel();
    //show signup page 
    this.showLogin= false;
    this.showSignup = true;
  }

  hideSignUp(){
    this.userModel = new MobUserLoginModel();
    this.showLogin= true;
    this.showSignup = false; 
    }

    backToSignup(){
      this.userModel = new MobUserLoginModel();
      this.showverify =false;
   this.showSignup = true;
   this.showLogin= false;

    }

}
