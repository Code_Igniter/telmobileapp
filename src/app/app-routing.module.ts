import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { doctorbookPage } from './doctor/doctorbook.page';
import { HomePage } from './home/home.page';
import { LoginPage } from './login/login.page';
import { PaymentPage } from './Payment/Payment.page';
 import { WaitingroomPage } from './Waitingroom/waitingroom.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'doctorbook',
    component: doctorbookPage
  },
  {
    path: 'waitingroom',
    component: WaitingroomPage
  },
  {
    path: 'payment',
    component: PaymentPage
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'home',
    component: HomePage
  },
  {
    path: 'pat_history',
    loadChildren: () => import('./patient/pathistory/patient_history.module').then( m => m.PatienthistoryPageModule)
  },
  {
    path: 'pat_bookinglist',
    loadChildren: () => import('./patient/allbookinglist/patient_bookinglist.module').then( m => m.BookinglistPageModule)
  },
  {
    path: 'pat_profile',
    loadChildren: () => import('./patient/patprofile/patprofile.module').then( m => m.PatientprofileModule)
  },
  {
    path: 'book_appointment',
    loadChildren: () => import('./patient/bookappointment/book_appointment.module').then( m => m.BookAppointmentPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
