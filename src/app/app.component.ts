import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [  
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },  
     {
      title: 'Book Appointment',
      url: '/book_appointment',
      icon: 'create'
    },  
     {
      title: 'Profile',
      url: '/pat_profile',
      icon: 'person'
    },  
     {
      title: 'Booking List',
      url: '/pat_bookinglist',
      icon: 'list'
    },       
    {
      title: 'History',
      url: '/pat_history',
      icon: 'book'
    },
    {
      title: 'Logout',
      url: '/login',
      icon: 'log-out'
    },
   
  ];
 
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,private androidPermissions: AndroidPermissions
  ) {
    this.initializeApp();
    this.gotologin();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.androidPermissions.requestPermissions(
        [
          this.androidPermissions.PERMISSION.CAMERA, 
          this.androidPermissions.PERMISSION.CALL_PHONE, 
          this.androidPermissions.PERMISSION.GET_ACCOUNTS, 
          this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
          this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
        ]
      );
    });
  
  }

  ngOnInit() {
    const path = window.location.pathname.split('login')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
  gotologin()
  {
    this.router.navigate(['login']);
  }
}
