import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { Patient } from '../models/patient/patient.model';
import { AuthenticationService } from '../services/authentication.service';
import { DoctorService } from '../services/doctor/doctor.service';
import { MessageService } from '../services/message-service/message-service';
import { PatientService } from '../services/patient/patient.service';
//import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public folder: string;
  public token: any;
  public patInfo: Patient = new Patient();
  public deptList: any[];
  public doctorlist: any;

  constructor(private activatedRoute: ActivatedRoute, public menueCtrl: MenuController, public patservice: PatientService, private router: Router,
    public authenticationService: AuthenticationService, public docservice: DoctorService, public messageService: MessageService,
  ) {
    this.token = this.authenticationService.currentUserValue;
    this.getPatientinfo();
    this.GetDepartmentList();
    this.getDoctorList();
  }
  //enable side menue
  ionViewWillEnter() {
    this.menueCtrl.enable(true);
  }

  public getPatientinfo() {
    this.patservice.getPatientinfo(this.token.PatientIdentifier).subscribe(res => this.Success(res),
      res => this.Error(res));
  }
  Success(res) {
    this.patInfo = res;
  }
  Error(res) {
  }

  GetDepartmentList() {
    this.docservice.getDepartmentList().subscribe(res => {
      this.deptList = null;
      this.deptList = res;

    },
      error => {
        this.messageService.errorMessage(error);
      });
  }
  getDoctorList() {
    this.docservice.getDoctorList().subscribe(res => this.getdrlistSuccess(res),
      res => this.getdrlistFailedError(res));
  }
  getdrlistSuccess(res) {
    this.doctorlist = null;
    this.doctorlist = res;
  }
  getdrlistFailedError(res) {
    this.messageService.errorMessage(res);
  }

  ngOnInit() {

  }

  gotobookappointment() {
    this.router.navigate(['book_appointment']);
  }

}
