import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';
import { PatientService } from '../services/patient/patient.service';

import { MessageService } from 'src/app/services/message-service/message-service';
import { Patient } from 'src/app/models/patient/patient.model';
import { NgForm } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DoctorService } from '../services/doctor/doctor.service';
import { HospitalService } from '../services/hospital/hospital.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Doctor } from '../models/doctor/doctor.model';
import { Departement } from '../models/Department/app.department.model';
import { Visit } from '../models/visit/visit.model';
import { HospitalDoctorMap } from '../models/HospitalDoctorMap/app.hospitaldoctormap.model';
import { SchedulingList } from '../models/scheduling/scheduling.model';
import * as moment from 'moment';
import { SchedulingService } from '../services/scheduling/scheduling.service';
import { DatePipe } from '@angular/common';
import { Global } from '../app.global';
import { Hospital } from '../models/hospital/hospital.model';


@Component({
  selector: 'app-home',
  templateUrl: './doctorbook.page.html',
  styleUrls: ['./doctorbook.page.scss'],
})
export class doctorbookPage  {
  // public doctorId : any;
//  public  QualificationList: Array<any> = new Array<any>();
//  public hospitalList: Array<any>;
//  public selHospital: any;
//  public hospitalId :any;
//  public showDept: boolean = false;
//  DoctorName: any;
//  FilePath: any;
//  PhoneNumber: any;
//  MailingAddress: any;
  // public token: any;
  // public EnablePhNo: any;


  public docInfo: Doctor = new Doctor();
  public departmentId: any;
  public filterDocList: any;
  public selDepartment: any = "";
  public count: number = 0;
  public docHospitalList: Array<any>;
  public HospitalList: Array<any>;
  public dept: Departement = new Departement();
  visitObj: Visit = new Visit();
  public showDept: boolean = false;
  public showUnavailableMsg: boolean = false;
  appointmentTime: Array<any> = new Array<any>();
  scheduleIntervalList: Array<any> = new Array<any>();
  // VisitDate[] = [];
  public deptList: any[];
  public filterdeptList: Array<any> = new Array<any>();
  public filteredDepartmentList: Array<any> = new Array<any>();
  public docList: Array<Doctor> = new Array<Doctor>();
  public showScheduleList : boolean = false ;
  loading = false;
  public doc: any;
  public token: any;
  public hospitalList: Array<any>;
  public DeptHostList: any;
  public selHospital: any;
  public VisitList: any;
  public hospitalId: any;
  public EnablePhNo: any;
  public showBookingDate: boolean = false;
  public showTimeSlot: boolean = false;
  public ScheduleTimeDetail: boolean = true;
  public showIntervalSlot: boolean = false;
  public showIntervalButton: boolean = false;
  public showBookingAppointBtn: boolean = false;
  public AppointmentDate: any = "";
  public AppointmentStartTime: any = "";
  public AppointmentEndTime: any = "";
  public doctorId: any;
  public patId: any;
  public rowCount: number = 0;
  public index: number = 0;
  public hospdocMap: Array<HospitalDoctorMap> = new Array<HospitalDoctorMap>();
  public doctorList: Array<any> = new Array<any>();
  public deptId: any;
  DoctorName: any;
  HospitalName : any;
  FilePath: any;
  PhoneNumber: any;
  MailingAddress: any;
  todayDate:any;
  public currentDate = moment().format('MM-DD-YYYY');
  
  QualificationList: Array<any> = new Array<any>();
  SchedulingList: Array<SchedulingList> = new Array<SchedulingList>();
  filteredHospitalDoctorMapping: Array<any> = new Array<any>();
  tempArray: Array<any> = new Array<any>();
  filteredSchedulingList: Array<any> = new Array<any>();
  HospitalDoctorMapping: Array<any> = new Array<any>();
  ScheduleList: Array<any> = new Array<any>();
  hospital: Hospital = new Hospital();
  selectedValue = "Tele";
  

  constructor(private activatedRoute: ActivatedRoute, public formBuilder: FormBuilder, public menueCtrl: MenuController, public patservice: PatientService,
    public messageService: MessageService, public authenticationService: AuthenticationService,public docService: DoctorService,public hospitalService :HospitalService
 
    ,public global: Global, public docservice: DoctorService,  public route: ActivatedRoute,
    public router: Router, public httpClient: HttpClient, public patService: PatientService, public schedulingService: SchedulingService,
    public http: HttpClient, private datepipe: DatePipe, 
   
 
    ) {
    this.todayDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
   // this.visitObj.VisitDate = this.todayDate;
    this.token = this.authenticationService.currentUserValue;
    this.doctorId = this.route.snapshot.queryParamMap.get('id1');
    if (this.doctorId == "" || this.doctorId == null) {
      this.doctorId = this.docService.DoctorId;
    }

    this.token = this.authenticationService.currentUserValue;
   this.doctorId = this.docService.DoctorId
    this.getDocHospitalList();
   
  }
  getDocHospitalList() {
    this.hospitalService.getDocHospitalList(this.doctorId)
      .subscribe(res => this.SuccessHospitalList(res),
        res => this.Error(res));
  }
  SuccessHospitalList(res) {
    this.hospitalList = res;
  //  if (this.data.hospitalId){
      console.log()
      // this.hospitalList = this.hospitalList.filter(s => s.HospitalId == this.data.hospitalId)
      this.selHospital = this.hospitalList[0].HospitalId ;
      this.hospitalId = this.selHospital ;
      this.showDept = true ;
      this.GetDepartmentList();
 //   }
    if (this.hospitalList.length == 1) {
      this.QualificationList = this.hospitalList[0].Qualification;
      this.DoctorName = this.hospitalList[0].DoctorName;
      this.HospitalName = this.hospitalList[0].HospitalName;
      this.FilePath = this.hospitalList[0].FilePath;
      this.PhoneNumber = this.hospitalList[0].PhoneNumber;
      this.EnablePhNo = this.hospitalList[0].EnablePhNo;
      this.MailingAddress = this.hospitalList[0].MailingAddress;
      this.selHospital = this.hospitalList[0].HospitalId;
      this.hospitalId = this.selHospital;
     // this.global.HospitalId = this.hospitalId;
      this.hospitalService.HospitalId = this.hospitalId;
      this.showDept = true;
       this.GetDepartmentList();
      this.filteredSchedulingList = null;
    }
    else {
      this.QualificationList = this.hospitalList[0].Qualification;
      this.DoctorName = this.hospitalList[0].DoctorName;
      this.HospitalName = this.hospitalList[0].HospitalName;
      this.FilePath = this.hospitalList[0].FilePath;
      this.PhoneNumber = this.hospitalList[0].PhoneNumber;
      this.EnablePhNo = this.hospitalList[0].EnablePhNo;
      this.MailingAddress = this.hospitalList[0].MailingAddress;
      this.selHospital = "";
    }
    
    
  }
  Error(res) {
  }
  




  //enable side menue
  ionViewWillEnter() {
    this.menueCtrl.enable(true);
  }

  
  AssignHospital(event) {
    //this.hospitalService.HospitalId = event.value;
    this.appointmentTime = [];
    this.visitObj.BookingTime = null;
    this.hospitalId = event.value;
    this.showDept = true;
    this.showTimeSlot = false ;
    this.showScheduleList = false ;
    this.visitObj.VisitDate = null ;
    this.showUnavailableMsg = false ;
    this.GetDepartmentList();
    
  }

  GetDoctorList() {
    this.docservice.getDoc(this.doctorId, this.hospitalId)
      .subscribe(res => this.SuccessDoctor(res),
        res => this.getDoctorError(res));
  }
  SuccessDoctor(res) {
    this.doctorList = res;
    this.SchedulingList = res.SchedulingList;
    this.tempArray = new Array<any>();
    this.tempArray = JSON.parse(JSON.stringify(this.SchedulingList));
    if (this.tempArray.length == 0) {
      this.showUnavailableMsg = true;
    }
    for (var y = 0; y < this.tempArray.length; y++) {
      var tempst = 0;
      tempst = +this.tempArray[y].StartTime.substr(0, 2);
      if (tempst < 12) {
        var stampm = "AM";
        var ST = tempst;
      }
      else {
        stampm = "PM";
        if (tempst == 12) {
          var ST = tempst;
        } else {
          ST = tempst - 12;
        }
      }
      this.tempArray[y].StartTime = ST + this.tempArray[y].StartTime.substr(2, 3) + " " + stampm;
      var tempet = 0;
      tempet = +this.tempArray[y].EndTime.substr(0, 2);
      if (tempet < 12) {
        var etampm = "AM";
        var ET = tempet;
      }
      else {
        etampm = "PM";
        if (tempet == 12) {
          var ET = tempet;
        } else {
          ET = tempet - 12;
        }
      }
      this.tempArray[y].EndTime = ET + this.tempArray[y].EndTime.substr(2, 3) + " " + etampm;
    }
    this.SchedulingList = this.tempArray;
    this.filteredSchedulingList = res.SchedulingList;
    this.tempArray = new Array<any>();
    this.tempArray = JSON.parse(JSON.stringify(this.filteredSchedulingList));
    for (var y = 0; y < this.tempArray.length; y++) {
      var tempst = 0;
      tempst = +this.tempArray[y].StartTime.substr(0, 2);
      if (tempst < 12) {
        var stampm = "AM";
        var ST = tempst;
      }
      else {
        stampm = "PM";
        if (tempst == 12) {
          var ST = tempst;
        } else {
          ST = tempst - 12;
        }
      }
      this.tempArray[y].StartTime = ST + this.tempArray[y].StartTime.substr(2, 3) + " " + stampm;
      var tempet = 0;
      tempet = +this.tempArray[y].EndTime.substr(0, 2);
      if (tempet < 12) {
        var etampm = "AM";
        var ET = tempet;
      }
      else {
        etampm = "PM";
        if (tempet == 12) {
          var ET = tempet;
        } else {
          ET = tempet - 12;
        }
      }
      this.tempArray[y].EndTime = ET + this.tempArray[y].EndTime.substr(2, 3) + " " + etampm;
    }
    this.filteredSchedulingList = this.tempArray;
    this.HospitalDoctorMapping = res.HospitalDoctorMapId;
  }
  getDoctorError(res) {

  }
  GetDepartmentList() {
    this.docservice.getDepartmentList().subscribe(res => {
      this.deptList = res;
      this.GetDocHosDetail();
    },
      error => {
        this.messageService.errorMessage(error);
        this.loading = false;
      });
  }
  GetDocHosDetail() {
    this.hospitalService.getDocHospitalList(this.doctorId)
      .subscribe(res => this.SuccessHospital(res),
        res => this.Error(res));
  }
  SuccessHospital(res) {
    this.filterdeptList = [];
    this.count = 0;
    this.docHospitalList = res;
    this.docHospitalList = this.docHospitalList.filter(s => s.HospitalId == this.hospitalId);
    for (var i = 0; i < this.deptList.length; i++) {
      for (var j = 0; j < this.deptList.length; j++) {
        if (this.deptList[i].DepartmentId == this.docHospitalList[0].DepartmentId[j]) {
          this.filterdeptList[this.count] = this.deptList[i];
          this.count++;
        }
      }
    }
    this.filteredDepartmentList = this.filterdeptList;

    if (this.filteredDepartmentList.length == 1) {

      this.selDepartment = this.filteredDepartmentList[0].DepartmentId;
      this.deptId = this.selDepartment;
      this.global.DepartmentId = this.deptId;
      this.hospitalService.DepartmentId = this.deptId;
      this.showBookingDate = true;
      this.showScheduleList = true ;
      //this.filteredSchedulingList = null;
    }
    else {
      this.selDepartment = "";
    }
    this.GetDoctorList();
  }
  AssignDepartment(event) {
    this.showUnavailableMsg = false;
    this.hospitalService.DepartmentId = event.value;
    this.deptId = event.value;
    this.showBookingDate = true;
    this.showTimeSlot = false ;
    this.showScheduleList = true ;
    this.appointmentTime = [];
    this.visitObj.BookingTime = null;
    this.visitObj.VisitDate = null ;
    this.showBookingAppointBtn = false;
    this.filteredHospitalDoctorMapping = new Array<any>();
    this.filteredHospitalDoctorMapping = JSON.parse(JSON.stringify(this.HospitalDoctorMapping));//deepcopy
    this.filteredHospitalDoctorMapping = this.filteredHospitalDoctorMapping.filter(s => s.DepartmentId == event.value);
    this.filteredSchedulingList = new Array<any>();
    this.filteredSchedulingList = JSON.parse(JSON.stringify(this.SchedulingList));//deepcopy
    this.filteredSchedulingList = this.filteredSchedulingList.filter(s => s.HospitalDoctorMapId == this.filteredHospitalDoctorMapping[0].HospitalDoctorMapId);
    if (this.filteredSchedulingList.length == 0) {
      this.showUnavailableMsg = true;
    }
   
    //for (var y = 0; y < this.filteredSchedulingList.length; y++) {
    //  var tempst = 0;
    //   tempst = +this.filteredSchedulingList[y].StartTime.substr(0, 2);
    //  if (tempst < 12) {
    //    var stampm = "AM";
    //    var ST = tempst;
    //  }
    //  else
    //  {
    //    stampm = "PM";
    //    if (tempst == 12) {
    //      var ST = tempst;
    //    } else {
    //      ST = tempst - 12;
    //    }
    //  }
    //  this.filteredSchedulingList[y].StartTime = ST + this.filteredSchedulingList[y].StartTime.substr(2, 3) + " " + stampm;
    //  var tempet = 0;
    //  tempet = +this.filteredSchedulingList[y].EndTime.substr(0, 2);
    //  if (tempet < 12) {
    //    var etampm = "AM";
    //    var ET = tempet;
    //  }
    //  else {
    //    etampm = "PM";
    //    if (tempet == 12) {
    //      var ET = tempet;
    //    } else {
    //      ET = tempet - 12;
    //    }
    //  }
    //  this.filteredSchedulingList[y].EndTime = ET + this.filteredSchedulingList[y].EndTime.substr(2, 3) + " "+ etampm;
    //}
    //for (var b = 0; b < this.filteredSchedulingList.length; b++) {
    //  this.filteredSchedulingList[b].Date = moment(this.filteredSchedulingList[b].Date).format('YYYY-MM-DD');
    //  var todaydate = new Date();
    //  if (this.filteredSchedulingList[b].Date == moment(todaydate).format('YYYY-MM-DD')) {
    //    var currentdate1 = moment(todaydate).format('YYYY/MM/DD');
    //    var timeextract = this.filteredSchedulingList[b].EndTime;
    //    var bookedDateTime = moment(currentdate1 + ' ' + timeextract, 'YYYY/MM/DD HH:mm A');
    //    var formatedBookedDateTime = moment(bookedDateTime, 'YYYY-MM-DD HH:mm A');
    //    var currentTime = moment(todaydate).format('YYYY-MM-DD HH:mm A');
    //    var momentObj = moment(currentTime, 'YYYY-MM-DDLT');
    //    var currentdateTime = momentObj.format('YYYY-MM-DDTHH:mm:s');
    //    var formatedCurrentTime = moment(currentdateTime, 'YYYY-MM-DD HH:mm A');
    //    if (formatedCurrentTime >= formatedBookedDateTime) {
    //      this.filteredSchedulingList.splice(b , 1);
    //    }
    //  }

    //}
  }
  GetVisitTime(event) {
    this.ScheduleTimeDetail = true;
    this.showIntervalSlot = false;
    this.showIntervalButton = true;
    this.appointmentTime = [];
    this.visitObj.BookingTime = null;
    this.showBookingAppointBtn = false;
    // this.visitObj.VisitDate = null ;
    //var vDate = moment(event.value);
    var vDate = moment(this.visitObj.VisitDate);
    var visitDate = vDate.format('YYYY-MM-DD');
    if (visitDate != null) {
      this.schedulingService.getVisitTime(visitDate, this.doctorId, this.deptId, this.hospitalId)
      .subscribe(res => this.SuccessAppointmentTime(res),
        res => this.ErrorAppointmentTime(res));
    }
  }
  SuccessAppointmentTime(res) {
    this.appointmentTime = Object.assign(this.appointmentTime, res);
    if (this.appointmentTime.length == 0) {
      this.messageService.waringMessage("Sorry There is no schedule for" + " " + "Dr." + " " + `${this.DoctorName}` + " " + "on" + " " + `${moment(this.visitObj.VisitDate).format('YYYY-MM-DD')}`);
      console.log("No Schedule");
      this.showTimeSlot = false;
    }
    if (this.doctorId != null && this.appointmentTime.length > 0) {
      this.showTimeSlot = true;
      
      //this.appointmentTime = this.appointmentTime;
      //this.appointmentTime.filter(x => x.DoctorId == this.doctorId && x.DepartmentId == this.hospitalService.DepartmentId && x.HospitalId == this.hospitalId);
     // this.notifyService.showInfo("Please", "Select the time slot !");
    }
    else if (this.doctorId == null && this.appointmentTime.length > 0) {
      this.appointmentTime = this.appointmentTime.filter(x => x.DoctorId == this.doctorId && x.DepartmentId == this.selDepartment && x.HospitalId == this.hospitalId);
     }
    else {
      this.appointmentTime = [];
    }
  }
  ErrorAppointmentTime(res) {
    this.messageService.waringMessage("Sorry There is no schedule for" + " " + "Dr." + " " + `${this.DoctorName}` + " " + "on" + " " + `${moment(this.visitObj.VisitDate).format('YYYY-MM-DD')}`);
  }
  setAppontmentTime(time) {
    var date = new Date();
    var currenttime = moment(date, 'YYYY-MM-DDLT');
    if (time.StartTimeDetail > currenttime) {
      if (time.IsBooked == true) {
        this.ScheduleTimeDetail = true;
        var bookingtime = `${time.StartTime} - ${time.EndTime}`;
        this.messageService.waringMessage("The time slot " + bookingtime + " is already booked! Sorry");
      }
      else {
        this.ScheduleTimeDetail = false;
        this.patId = this.token.PatientIdentifier == undefined ? this.patService.PatientId : this.token.PatientIdentifier;
        this.patService.getPatientHistory(this.patId)
          .subscribe(res => this.PatientHistory(res, time),
            res => this.Error(res));
      }
    }
    else {
      this.messageService.waringMessage("You cannot book past Appointment Time");
    }
    
  }
  PatientHistory(res, time) {
    this.VisitList = res;
    var count = 0;
    for (var i = 0; i < this.VisitList.length; i++) {
      if (this.VisitList[i].SchedulingId == time.SchedulingId) {
        count++;
      }
    }
    if (count == 0) {
      this.visitObj.BookingTime = `${time.StartTime} - ${time.EndTime}`;
      this.global.BookingTime = this.visitObj.BookingTime;
      this.global.VisitDate = this.visitObj.VisitDate;
      this.docservice.ScheduleIntervalId = time.ScheduleIntervalId;
      this.docservice.SchedulingId = time.SchedulingId;
      this.showBookingAppointBtn = true;
    } else
    {
      this.ScheduleTimeDetail = true;
      this.messageService.waringMessage("You can't select another schedule on same interval ");
    }
    
  }
  checkTimeSlot(SchId) {
    this.visitObj.BookingTime = null;
    this.showBookingAppointBtn = false;
    if (this.showIntervalSlot == true) {
      this.scheduleIntervalList = [];
      this.showIntervalSlot = false;
    } else {
    this.schedulingService.getScheduleIntervalBySchedulingId(SchId).subscribe(res => this.SuccessScheduleInterval(res),
      res => this.Error(res));
    }
  }
  SuccessScheduleInterval(res) {
    this.scheduleIntervalList = Object.assign(this.scheduleIntervalList, res);
    for (var c = 0; c < this.scheduleIntervalList.length; c++ ) {
      var date = moment(this.scheduleIntervalList[c].Date).format('YYYY/MM/DD');
      var timeextract = this.scheduleIntervalList[c].StartTime;
      var bookedStartTime = moment(date + ' ' + timeextract, 'YYYY/MM/DD HH:mm A');
      var momentObj = moment(bookedStartTime, 'YYYY-MM-DDLT');
      var currentdateTime = momentObj.format('YYYY-MM-DDTHH:mm:s');
      var formatedBookedDateTime = moment(currentdateTime, 'YYYY-MM-DD HH:mm A');
      //var formatedBookedDateTime =moment(moment(bookedStartTime, 'YYYY-MM-DDTHH:mm:s')).format('lll');
      //var formatedBookedDateTime = moment(BookedDateTime); 
      this.scheduleIntervalList[c].StartTimeDetail = formatedBookedDateTime;
    }
    console.log(this.scheduleIntervalList);
    
    for (let k = 0; k < this.scheduleIntervalList.length - 1; k++) {
      for (let l = 0; l < this.scheduleIntervalList.length- k- 1; l++) {
       var diffmin = this.scheduleIntervalList[l + 1].StartTimeDetail.diff((this.scheduleIntervalList[l].StartTimeDetail), 'minutes')
        if (diffmin < 0) {
          let swap = this.scheduleIntervalList[l];
          this.scheduleIntervalList[l]= this.scheduleIntervalList[l+ 1];
          this.scheduleIntervalList[l +1]= swap;
        }
      }
    }

    this.showIntervalSlot = true;
    console.log(this.scheduleIntervalList);
  }
  SelectSchedule(data) {
    this.ScheduleTimeDetail = true;
    this.showIntervalButton = false;
    this.showBookingAppointBtn = false;
    this.visitObj.VisitDate = data.Date;
    this.scheduleIntervalList = [];
    if (this.showIntervalSlot == true) {
      this.showIntervalSlot = false
    } 
    this.appointmentTime = [];
    this.visitObj.BookingTime = null;
    //this.visitObj.VisitDate = null ;
    this.appointmentTime.push({
      VisitDate: data.Date,
      StartTime: data.StartTime,
      EndTime: data.EndTime,
      SchedulingId: data.SchedulingId,
    })
    this.checkTimeSlot(data.SchedulingId);
    this.SuccessAppointmentTime(this.appointmentTime);
    //this.setAppontmentTime(time);
  }

  public BookApoointment() {
    if (this.appointmentTime.length > 0)
    {
      if (this.token.PatientIdentifier == undefined) {
        this.patId = this.patService.PatientId;
      } else {
        this.patId = this.token.PatientIdentifier;
      }
      this.visitObj.PatientId = this.patId;
      this.docService.DoctorId = this.doctorId != undefined ? this.doctorId : this.global.DoctorId;
      this.visitObj.ProviderId =  this.docService.DoctorId != null?  this.docService.DoctorId : this.global.DoctorId ;
    //  // this.visitObj.Docy = this.docId  != null? this.docId : this.docService.DoctorId ;
      this.visitObj.DepartmentId =this.hospitalService.DepartmentId != null ? this.hospitalService.DepartmentId : this.global.DepartmentId;
      this.hospitalService.DepartmentId = this.departmentId;
    //  //this.visitObj.DepartmentId = this.doctorId  != null? this.doctorId : this.selDoctor ;
      this.visitObj.HospitalId = this.hospitalId;
      if (this.hospital.PaymentEnable == false) {
        this.visitObj.PaymentStatus = "free";
        this.hospitalService.PaymentEnable = false;
      }
      else
      {
        this.visitObj.PaymentStatus = "unpaid";
      }
      this.visitObj.Status = "initiated";
     this.visitObj.VisitType = this.selectedValue;
      //let mdt = moment([this.visitDate.year, this.visitDate.month, this.visitDate.day]);
      //this.visitObj.VisitDate = new Date(mdt.format('MM/DD/YYYY'));
  
     // this.visitObj.PatientFiles = Object.assign(this.visitObj.PatientFiles, this.patFiles);
      this.patService.bookappointment(this.visitObj, this.docService.ScheduleIntervalId, this.docService.SchedulingId).subscribe(res => this.SuccessPostProblem(res),
        res => this.PostProblemError(res));
    }
    else{
     // this.notifyService.showError("Error","Please select the doctor at top and proceed!");
     this.messageService.waringMessage("Sorry There is no schedule for" + " " + "Dr." + " " + `${this.DoctorName}` + " " + "on" + " " + `${moment(this.visitObj.VisitDate).format('YYYY-MM-DD')}` + "Please search another Date for Appointment");
   
    }
    
  }
  SuccessPostProblem(res) {
    this.hospital.HospitalId= null;
    this.docService.DoctorId = null;
    this.hospitalService.DepartmentId = null;  
    this.docservice.DoctorId = null;
    this.global.BookingTime = "";
    this.global.VisitDate = null;
    this.visitObj.DepartmentId = null;
    this.global.DepartmentId = null;
    this.router.navigate(['/pat_bookinglist']);
    this.messageService.successMessage("Appointment booked Successfully");
   

   
  }
  PostProblemError(res) {
    this.messageService.errorMessage("Please fill up the required field")
  }
  // public BackDashBoard() {
  //   this.hospitalService.DepartmentId = null;
  //   this.docservice.DoctorId = null;
  //   this.global.BookingTime = "";
  //   this.global.VisitDate = null;
  //   this.visitObj.DepartmentId = null;
  //   this.global.DepartmentId = null;
  //   this.router.navigate(['/HospitalList']);
  // }
  public BackBookAppointment(){
    window.location.reload();
    //this.router.navigate(['/PatDashboard']);
  }
  backtobookpage ()
  {
    this.router.navigate(['book_appointment']);
  }
 



}
