import { Component,ViewChild, OnInit ,ElementRef,ChangeDetectorRef} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';
import { PatientService } from '../services/patient/patient.service';
import { MessageService } from 'src/app/services/message-service/message-service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DoctorService } from '../services/doctor/doctor.service';
import { HospitalService } from '../services/hospital/hospital.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Global } from '../app.global';
import { timer } from 'rxjs';
import { Visit } from '../models/visit/visit.model';
import { Patient, PatientDoc } from '../models/patient/patient.model';
import { ChatService } from '../services/chat-notification.service';
import { takeWhile } from 'rxjs/operators';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Platform } from '@ionic/angular';
import { OpentokService } from '../services/opentok.service';
import { AfterViewInit } from '@angular/core';
import 'src/vendor/jitsi/external_api.js';

declare var OT:any;

@Component({
  selector: 'app-home',
  templateUrl: './waitingroom.page.html',
  styleUrls: ['./waitingroom.page.scss'],
})
export class WaitingroomPage implements OnInit,AfterViewInit{
  // session: any;
  // publisher: any;
  // apiKey: any;
  // sessionId: string;
  // apptoken: string;
  //tokbox
  session: OT.Session;
  streams: Array<OT.Stream> = [];
  changeDetectorRef: ChangeDetectorRef;
 
  public patientName: string;
  timer1: any = timer(1000, 2000);
  apiStatusDestroy= false;
  apiPatDocDestroy = false;
  redUrl: string;
  token: any;
  VisitId: string;
  status: any;
  loginVal: any;
  public index: number;
  patients: Array<PatientDoc> = new Array<PatientDoc>();
  visitObj: Visit = new Visit();
  HospitalId: string;
  doctorId: string;
  istimer: boolean = true;
  timerpat = timer(4000, 4000);
  timerdoc = timer(4000, 4000);
  apiDestroy: boolean = false;
  showincommingcall : boolean = false;
  roomName : any;
  remoteUserDisplayName :any;


  constructor(public httpClient: HttpClient,private opentokService: OpentokService,private ref: ChangeDetectorRef,
    patservice: PatientService, private route: ActivatedRoute, public docService: DoctorService,
    public routing: Router, private authenticationService: AuthenticationService,public formBuilder: FormBuilder, 
    public global: Global,public hospitalService :HospitalService, public patService: PatientService,public notificationService : ChatService,
    private diagnostic: Diagnostic,
    private platform: Platform, public messageservice : MessageService){
      
      this.token = this.authenticationService.currentUserValue;
      this.VisitId = this.patService.VisitId;
      this.HospitalId = this.hospitalService.HospitalId;
      this.redUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
      this.doctorId = this.docService.DoctorId;
      this.GetEntrytoDoctorRoom();
   
      this.GetPatientDoctorList();
     
      this.changeDetectorRef = ref;    
  }
  ngOnInit() {
    this.GetStatusByPatientId();
    
    }


  getPermission() {
    console.log('getPermission');
    this.diagnostic.requestCameraAuthorization()
      .then((res) => {
        console.log('res 0');
        console.log(res);
        return this.diagnostic.requestMicrophoneAuthorization()
      })
      .then((res) => {
        console.log('res');
        console.log(res);
      }).catch((err) => {
        console.log('err', err);
      }).finally(() => {
        console.log('finally');
       // this.webrtc();
     
      });
     
  }
  // startCall() {
  //   this.session = OT.initSession(this.apiKey, this.sessionId);
  //   this.publisher = OT.initPublisher('publisher');

  //   this.session.on({
  //     streamCreated: (event: any) => {
  //       this.session.subscribe(event.stream, 'subscriber');
  //       OT.updateViews();
  //     },
  //     streamDestroyed: (event: any) => {
  //       console.log(`Stream ${event.stream.name} ended because ${event.reason}`);
  //       OT.updateViews();        
  //     },
  //     sessionConnected: (event: any) => {
  //       this.session.publish(this.publisher);
  //     }
  //   });

  //   this.session.connect(this.apptoken, (error: any) => {
  //     if (error) {
  //       console.log(`There was an error connecting to the session ${error}`);
  //     }
  //   });
  // }
  GetEntrytoDoctorRoom() {
    this.timer1.pipe(
      takeWhile(x => this.apiStatusDestroy === false)
    ).subscribe(
      () => {
      //this.timer1.subscribe(
      //  () => {
        //if (this.VisitId == null) {
        //  this.patService.getStatusByPatientId(this.token.PatientIdentifier).subscribe(res =>
        //    //this.Success(res),
        //    this.SuccessStatusVisitId(res),
        //    res => this.Error(res));
        //}
        if (this.redUrl === "PatientUrl") {
          this.patService.getStatusByPatientUrl(this.token.PatientIdentifier).subscribe(res =>
            this.SuccessStatusVisitId(res),
            res => this.Error(res));
        }
        else if (this.redUrl === "DoctorUrl") {
          this.patService.getStatusByPatientUrl(this.token.PatientIdentifier).subscribe(res =>
            this.SuccessStatusVisitId(res),
            res => this.Error(res));
        }

        else {
            this.patService.getPatientByVisitId(this.VisitId).subscribe(res =>
              this.SuccessStatusVisitId(res),
              res => this.Error(res));
          }      
        }
      );
    
    
  }
 
  SuccessStatusVisitId(res) {
    this.status = res;
    if (this.status.Status == "ongoing" && this.status.IsActive == true) {
      this.patService.VisitId = this.status.VisitId;
      this.apiStatusDestroy = true;
      // this.routing.navigate(['/DoctorRoom'], { queryParams: { id: this.status.VisitId } });
      //dr call to pat then below code execute

      /*
      * tokbox
      */
       this.showincommingcall = true;
        //jitsi
      //   this.roomName ='Telemedicine';
      //  this.remoteUserDisplayName='TelUser'
      //create session
      this.opentokService.initSession().then((session: OT.Session) => {
        this.session = session;
        //next publisher ctreated and publish stream --code in publisher file.
        this.session.on('streamCreated', (event) => {     
            this.streams.push(event.stream);              
          this.changeDetectorRef.detectChanges();
        });
        this.session.on('streamDestroyed', (event) => {
          const idx = this.streams.indexOf(event.stream);
          if (idx > -1) {
            this.streams.splice(idx, 1);
            this.changeDetectorRef.detectChanges();
           
                  
            this.showincommingcall=false;    
            this.patService.changeStatus(this.VisitId).subscribe(res => 
              this.SuccessChangestatus(res),
            res => this.Error(res));
            //  let publis = this.opentokService.getpublisher();
            // // if(publis){
            //    this.session.unpublish(publis);
            //   this.opentokService.resetpublisher();
            //   if(this.session){
             this.session.disconnect();
            //   }
            // }
            this.routing.navigate(["/pat_bookinglist"]);
          }
        });
      })
      //cennect session
      .then(() => this.opentokService.connect())
      .catch((err) => {
        console.error(err);
        alert('Unable to connect. Make sure you have updated the config.ts file with your OpenTok details.');
      });
  

    }
    else if (this.status.Status == "initiated" && this.status.IsActive == true) {
      this.apiStatusDestroy = false;    
           }
    else if (this.status.Status === "completed" || (this.status.Status === "initiated" && this.status.IsActive == false)) {
      this.apiStatusDestroy = true;
    }
    else {
      this.apiStatusDestroy = true;
    }
  }
  Error(res) {
    console.log(res);
  }
  SuccessChangestatus(res){    
  }

  GetPatientDoctorList() {
    this.timer1.pipe(
      takeWhile(x => this.apiPatDocDestroy === false)
    ).subscribe(
      () => {
      //this.timer1.subscribe(
      //  () => {
          this.loginVal = this.authenticationService.loginValue;
          if (this.loginVal === "true") {
            this.patService.getPatientDocList(this.docService.DoctorId)
              .subscribe(res => this.SuccessPatientList(res),
                res => this.Error(res))
          };
        });
    }


  SuccessPatientList(res) {
    this.patients = res;
    this.index = this.patients.findIndex(x => x.VisitId === this.visitObj.VisitId);
    this.apiPatDocDestroy = true;
   // console.log(this.index);
  }

  TimerDocandPat() {
    if (this.token.UserType == "Patient" && this.istimer == true && this.apiDestroy == false) {
      this.timerpat.pipe(
        takeWhile(x => this.istimer == true && this.apiStatusDestroy === false)
      ).subscribe(
        () => {
          this.loginVal = this.authenticationService.loginValue;
          if (this.loginVal == "true" ) {
            this.patService.getStatus(this.patService.VisitId).subscribe(res =>
           //   this.StatusSuccess(res),
              res => this.Error(res));
          }
        }
      );


    }
    else {

      this.timerdoc.pipe(
        takeWhile(x => this.istimer == true)
      ).subscribe(
        () => {
          this.loginVal = this.authenticationService.loginValue;
          if (this.loginVal == "true") {
           // this.RefreshPatients();
           
          }
        });
    }
  }

  OnInit() {  
   
  //

  }

  GetStatusByPatientId() {
    this.patService.getStatusByPatientId(this.token.PatientIdentifier).subscribe(res =>
      this.SuccessResponse(res),
      res => this.Error(res));
  }
  SuccessResponse(res) {
    this.status = res;
    this.patientName = this.status.PatientName;
    if (this.VisitId == null) {
      var visitid = this.status.VisitId;
      this.patService.VisitId = this.status.VisitId;
      this.patService.getPatientByVisitId(visitid)
        .subscribe(res => this.SuccessPatientDetail(res, visitid),
          res => this.Error(res));
    }
    else {
      this.patService.getPatientByVisitId(this.VisitId)
        .subscribe(res => this.SuccessPatientDetail(res, this.VisitId),
          res => this.Error(res));
    }

  }
  SuccessPatientDetail(res, visitid) {
    this.visitObj = res;
    this.visitObj.IsActive = true;
    this.visitObj.Status = "initiated";
    this.patService.updateStatus(this.visitObj, visitid)
      .subscribe(res => this.Successupdatestatus(res),
      res => this.Error(res));
  }
  Successupdatestatus(res){
  
  }
  
  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    this.platform.ready().then(() => {
      console.log('ready');
      this.getPermission();
    });
  
  }

 
 

 
}
