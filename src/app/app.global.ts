
import { DomSanitizer } from '@angular/platform-browser';
import { Doctor } from './models/doctor/doctor.model';
import { Patient } from './models/patient/patient.model';
import { Visit } from './models/visit/visit.model';

export class Global {
    patientObj: Patient = null;
    doctorObj: Doctor = null;
    IsDoctor: boolean = false;
    DoctorId: string = null;
    HospitalId: string = null;
    DepartmentId: string = null;
    BookingTime: string = null;
    VisitDate: Date = null;
    ApiUrl: string = "Home/";
    config: any = null;
    TimerValue: number = 4000;
    constructor() {
        this.patientObj = new Patient();
        this.doctorObj = new Doctor();
    }


}
