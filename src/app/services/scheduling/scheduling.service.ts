import { Injectable, ChangeDetectorRef } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import * as _ from 'lodash';
import { SchedulingEndpoint } from './scheduling-endpoint.service';
import { Scheduling, VisitDate, ScheduleInterval } from '../../models/scheduling/scheduling.model';
import { Visit } from 'src/app/models/visit/visit.model';


@Injectable()
export class SchedulingService {
  constructor(private router: Router, private http: HttpClient, private schedulingEndPoint: SchedulingEndpoint) {
  }

  
  getScheduleIntervalBySchedulingId(SchedulingId?: string) {
    return this.schedulingEndPoint.getScheduleIntervalBySchedulingIdEndpoint<ScheduleInterval>(SchedulingId);
  }

  getTimeVisit(visitDate: string ) {
    return this.schedulingEndPoint.getTimeVisitList<VisitDate[]>(visitDate);
  }
  getVisitTime(visitDate: string ,docId ?: string , deptId ?: string ,hosId?: string) {
    return this.schedulingEndPoint.getVisitTimeList<VisitDate[]>(visitDate,docId,deptId,hosId);
  }
 
}
