
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { EndpointFactory } from '../endpoint-factory.service';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class HospitalEndpoint extends EndpointFactory {
  public apiUrl = environment.api_Url;
  
  private readonly _hospitalListUrl: string =  this.apiUrl + "/api/hospital/hospitalDetails";
  private readonly _hospitalUrl: string = this.apiUrl + "/api/hospital/getHospital";
  private readonly _hospitalDocUrl: string = this.apiUrl + "/api/hospital/getDocHospital";
  

  
  constructor(http: HttpClient, injector: Injector) {
    super(http, injector);
  }
  getHospitalListEndpoint<T>(): Observable<T> {
    let endpointUrl = `${this._hospitalListUrl}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getHospitalListEndpoint()));
      }));
  }
  geHospitalEndpoint<T>(HospitalId:string): Observable<T> {
    let endpointUrl = `${this._hospitalUrl}/${HospitalId}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.geHospitalEndpoint(HospitalId)));
      }));
  }

  getDocHospitalListEndpoint<T>(doctordId:string): Observable<T> {
    let endpointUrl = `${this._hospitalDocUrl}/${doctordId}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getDocHospitalListEndpoint(doctordId)));
      }));
  }




}
