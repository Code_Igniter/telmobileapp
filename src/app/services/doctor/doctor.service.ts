import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { DoctorEndpoint } from './doctor-endpoint.service';
import { Doctor } from '../../models/doctor/doctor.model';

// import { Departement } from 'src/app/models/Department/app.department.model';

import { PatientFilesUploadModel } from 'src/app/models/patient/pat-fileupload.model';
import { Departement } from 'src/app/models/Department/app.department.model';
import { HospitalDoctorMap } from 'src/app/models/HospitalDoctorMap/app.hospitaldoctormap.model';
@Injectable()
export class DoctorService {
  constructor(private router: Router, private http: HttpClient, private doctorEndPoint: DoctorEndpoint) {
  }
  public _DoctorId: string;
  get DoctorId(): string {
    return this._DoctorId;
  }
  set DoctorId(DoctorId: string) {
    this._DoctorId = DoctorId;
  }
  public _ScheduleIntervalId: string;
  get ScheduleIntervalId(): string {
    return this._ScheduleIntervalId;
  }
  set ScheduleIntervalId(ScheduleIntervalId: string) {
    this._ScheduleIntervalId = ScheduleIntervalId;
  }
  public _SchedulingId: string;
  get SchedulingId(): string {
    return this._SchedulingId;
  }
  set SchedulingId(SchedulingId: string) {
    this._SchedulingId = SchedulingId;
  }

  getDoc(doctorId?: string, hospitalId?: string) {
    return this.doctorEndPoint.getDocByIdEndpoint<Doctor>(doctorId, hospitalId);
  }

  getDoctorList() {
    return this.doctorEndPoint.getDoctorListEndpoint<Doctor[]>();
  }
  getDepartmentList() {
    return this.doctorEndPoint.getDepartmentListEndpoint<Departement[]>();
  }
  
  getHospitalDoctorMapList(hospitalId?: string) {
    return this.doctorEndPoint.getHospitalDoctorMapListEndpoint<HospitalDoctorMap>(hospitalId);
  }
  getDoctorName(doctorId?: string) {
    return this.doctorEndPoint.getDoctorNameByIdEndpoint<Doctor>(doctorId);
  }
 
}
