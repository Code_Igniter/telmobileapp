
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { EndpointFactory } from '../endpoint-factory.service';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class DoctorEndpoint extends EndpointFactory {
  public apiUrl = environment.api_Url;

  private readonly _doctorlistUrl: string = this.apiUrl + "/api/doctor/getList";
  private readonly _departmentlistUrl: string = this.apiUrl + "/api/doctor/getDeptList";
  private readonly _doctorUrl: string =  this.apiUrl + "/api/doctor/getDoc";
  private readonly _hosdocListUrl: string =  this.apiUrl + "/api/doctor/getHosDocList";
  private readonly _doctorNameUrl: string =  this.apiUrl + "/api/doctor/getDocName";
  

  constructor(http: HttpClient, injector: Injector) {
    super(http, injector);
  }

  getDocByIdEndpoint<T>(doctorId: string, hospitalId: string): Observable<T> {
    let endpointUrl = `${this._doctorUrl}/${doctorId} /${hospitalId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getDocByIdEndpoint(doctorId, hospitalId)));
      }));
  }

  getDoctorListEndpoint<T>(): Observable<T> {
    let endpointUrl = `${this._doctorlistUrl}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getDoctorListEndpoint()));
      }));
  }

  getDepartmentListEndpoint<T>(): Observable<T> {
    let endpointUrl = `${this._departmentlistUrl}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getDepartmentListEndpoint()));
      }));
  }
  getHospitalDoctorMapListEndpoint<T>(hospitalId: string): Observable<T> {
    let endpointUrl = `${this._hosdocListUrl}/${hospitalId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getHospitalDoctorMapListEndpoint(hospitalId)));
      }));
  }
  getDoctorNameByIdEndpoint<T>(doctorId: string): Observable<T> {
    let endpointUrl = `${this._doctorNameUrl}/${doctorId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getDoctorNameByIdEndpoint(doctorId)));
      }));
  }

}
