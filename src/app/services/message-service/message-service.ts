import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})

export class MessageService {

  constructor(public toastController: ToastController) {

  }
  async errorMessage(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position:"middle",
      cssClass:"errorMessageCss",
  
    });
    toast.present();
  }
  async waringMessage(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position:"middle",
      cssClass:"waringMessageCss"
    });
    toast.present();
  }

  async successMessage(msg) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'middle',
      duration:5000,
      buttons: [
        {
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
          icon:'close',        
        }
      ],
      cssClass:'successMessageCss'
    });
    toast.present();
  }

  public ShowCatchErrMessage(exception) {
    if (exception) {
        let ex: Error = exception;
        this.waringMessage(ex.message);
        console.log("Error Messsage =>  " + ex.message);
        console.log("Stack Details =>   " + ex.stack);
        
    }
    else{
      this.waringMessage('please try again...');
    }
}

}