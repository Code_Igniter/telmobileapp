
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
@Injectable({
    providedIn: 'root'
})

export class LoadingService {
public loading;
  constructor(  public loadingController: LoadingController,) {

  }

  loaderToShow: any;
  public showLoader() {
    return this.loadingController.create({
                    spinner:"bubbles",
                    message: 'please wait...',
                    keyboardClose:true,
                  }).then((res) => {
                    res.present();
                  });
  }
 
  public hideLoader() {
   
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 1000);

  }

}