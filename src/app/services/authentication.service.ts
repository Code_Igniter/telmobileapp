import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { map } from 'rxjs/operators';
import { User } from '../models/user/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { decode } from 'querystring';
import { error } from 'protractor';
import { MessageService } from './message-service/message-service';
import jwt_decode from "jwt-decode";

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  userType: string;
  jwtToken: any;
  loginVal: any;
  result: boolean;
  constructor(private http: HttpClient, private route: ActivatedRoute, public messageService: MessageService,
    private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get loginValue() {
    let loginV = localStorage.getItem('login');
    this.loginVal = loginV
    return this.loginVal
  }
  public get currentUserValue(): User  {
    let token = sessionStorage.getItem('jwtToken');
    //if (token != null || token != undefined) {
      var decoded = jwt_decode(token);
      return this.jwtToken = decoded;
      
    //}
    //else {
    //  return "NoToken";
    //}

    //let token = this.currentUserSubject.value.token;
    //let token = this.currentUserSubject.value.token;
    //var decoded = jwt_decode(token);
  }

  login(phonenumber: string, password: string, isPatient: boolean) {
    return this.http.post<any>(`/api/account/login`, { phonenumber, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          sessionStorage.removeItem('jwtToken');
          //localStorage.setItem('currentUser', JSON.stringify(user));
          sessionStorage.setItem('jwtToken', user.token);
          localStorage.setItem('login', "true");
          let token = user.token;
          var decoded = jwt_decode(token);
          this.jwtToken = decoded;
         // this.userType = decoded.UserType;
          if (this.userType == "Patient") {
            this.router.navigate(['/HospitalList']);
            //this.currentUserSubject.next(user);
          }
          else if (this.userType == "Doctor") {
            this.router.navigate(['/DocDashboard']);
            //this.currentUserSubject.next(user);
          }

          else if (this.userType == "Admin") {
            this.router.navigate(['/admin']);
            this.messageService.successMessage("Admin Page")
            //("Info", "Admin Page")
          }
          //else {
          //  decoded.UserType = "admin"
          //  this.jwtToken = decoded;
          //  this.router.navigate(['/register']);
          //  this.notifyService.showInfo("Info", "Registration for new Doctor")
          //}

        }
        return user;
      }));
  }

  logout() {
    // remove user from session storage to log user out
    sessionStorage.removeItem('jwtToken');
    localStorage.removeItem('login');
    localStorage.setItem('login', "false");
    this.loginVal = "false";
    this.currentUserSubject.next(null);
  }

  public get IsTokenValid(): Observable<any> {
    
     return this.http.get<any>('/api/account/ValidUserCheck');
    //  .subscribe(res => {
    //    debugger;
    //    this.result = true;
    //    console.log(res)
    //  },
    //    (err: HttpErrorResponse) => {     
    //      if (err.status == 401) {
    //        debugger;
    //        this.result = false;
    //      }
    //    },
    //    () => console.log("Request Complete"));
    //return this.result;
  }
//  validateSession() {
//  let seassionVal = sessionStorage.getItem('jwtToken');
//  if (seassionVal !== null) {
//    let sessionObj = JSON.parse(seassionVal);
//    let expiredAt = new Date(value.expiredAt);
//    if (expiredAt > new Date()) { // Validate expiry date.
//      return sessionObj.value;
//    } else {
//      sessionStorage.removeItem(key);
//    }
//  }
//  return null;
//}

}
