
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { EndpointFactory } from '../endpoint-factory.service';
import { catchError, map, share } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';


@Injectable()
export class PatientEndpoint extends EndpointFactory {
 
  public apiUrl = environment.api_Url;
   private readonly _patbookinglistUrl: string = this.apiUrl + "/api/patient/patientHistory";   
   private readonly _patHistoryUrl: string = this.apiUrl +  "/api/patient/viewPrescription";
   private readonly _patientDetailsUrl : string = this.apiUrl + "/api/patient/getDetails";
   private readonly _patientUpdateUrl: string = this.apiUrl + "/api/patient/updatePatient";
   private readonly _hospitalListUrl: string =  this.apiUrl + "/api/hospital/hospitalDetails";
   private readonly _doctorlistUrl: string =  this.apiUrl + "/api/doctor/getList";
   private readonly _patientHistoryUrl: string =  this.apiUrl + "/api/patient/patientHistory";
   private readonly _bookappointmentUrl: string =  this.apiUrl +  "/api/patient/postProblem";
   private readonly _patientStatusByPatientUrl: string =  this.apiUrl +  "/api/patient/patientStatusByPatientUrl";
   private readonly _patientDetailsUrlByVisitId: string =  this.apiUrl + "/api/patient/getDetailsByVisitId";
   private readonly _patientListUrl: string =  this.apiUrl +  "/api/patient/listPatientByProviderId";
   private readonly _updateStatusUrl: string =  this.apiUrl +  "/api/patient/updateStatus";
   private readonly _patientTreatmentAdviceUpdateUrl: string = this.apiUrl + "/api/patient/updateTreatmentAdvice";
   private readonly _patientStatusByPatIdUrl: string =  this.apiUrl + "/api/patient/patientStatusByPatId";
   private readonly _patientStatusUrl: string =  this.apiUrl +  "/api/patient/patientStatus";
   private readonly _patientStatusChangeUrl: string =  this.apiUrl + "/api/patient/patientStatusChange";
   private readonly _updatePaidStatusUrl: string = this.apiUrl + "/api/patient/updatePaidStatus";
   private readonly _paymentChargeUrl: string = this.apiUrl + "/api/patient/paymentChargeByVisitId";

   constructor(http: HttpClient, injector: Injector) {
    super(http, injector);
  }
  
  getallbookinglistEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patbookinglistUrl}/${PatientId}`;

   return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
     catchError(error => {
       return throwError(this.handleError(error, () => this.getPatHistoryEndpoint(PatientId)));
     }));
 }
  getPatHistoryEndpoint<T>(PatientId: string): Observable<T> {
     let endpointUrl = `${this._patHistoryUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatHistoryEndpoint(PatientId)));
      }));
  }
  getPatientByIdEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientDetailsUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientByIdEndpoint(PatientId)));
      }));
  }

  
  getPatientHistoryEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientHistoryUrl}/${PatientId}`;

   return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
     catchError(error => {
       return throwError(this.handleError(error, () => this.getPatientHistoryEndpoint(PatientId)));
     }));
 }
 
  getUpdatePatientEndpoint<T>(userObject: any, PatientId?: string): Observable<T> {
    let endpointUrl = PatientId ? `${this._patientUpdateUrl}/${PatientId}` : this._patientUpdateUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getUpdatePatientEndpoint(userObject, PatientId)));
      }));
  }
  bookappointmentEndpoint<T>(userObject: any, ScheduleIntervalId?: string, SchedulingId?: string): Observable<T> {
    let endpointUrl = ScheduleIntervalId && SchedulingId ? `${this._bookappointmentUrl}/${ScheduleIntervalId}/${SchedulingId}` : this._bookappointmentUrl;
    return this.http.post<T>(endpointUrl, JSON.stringify(userObject ), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.bookappointmentEndpoint(userObject, ScheduleIntervalId, SchedulingId)));
      }));
  }
  getStatusByPatientUrlEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusByPatientUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusByPatientUrlEndpoint(PatientId)));
      }));
  }
  getPatientByVisitIdEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientDetailsUrlByVisitId}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientByVisitIdEndpoint(VisitId)));
      }));
  }
  getPatientDocListEndpoint<T>(DoctorId: string): Observable<T> {
    let endpointUrl = `${this._patientListUrl}/${DoctorId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientDocListEndpoint(DoctorId)));
      }));
  } 
  updateStatusEndpoint<T>(userObject: any, VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._updateStatusUrl}/${VisitId}` : this._patientTreatmentAdviceUpdateUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.updateStatusEndpoint(userObject, VisitId)));
      }));
  }
  getStatusByPatientIdEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusByPatIdUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusByPatientIdEndpoint(PatientId)));
      }));
  }
  getStatusEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusEndpoint(VisitId)));
      }));
  }
  changeStatusEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusChangeUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.changeStatusEndpoint(VisitId)));
      }));
  }
  updatePaidStatusEndpoint<T>(VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._updatePaidStatusUrl}/${VisitId}` : this._updatePaidStatusUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(VisitId), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.updatePaidStatusEndpoint(VisitId)));
      }));
  }
  getPaymentChargeEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._paymentChargeUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPaymentChargeEndpoint(VisitId)));
      }));
  }
}

