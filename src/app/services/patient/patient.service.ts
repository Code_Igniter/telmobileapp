import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {  PatientEndpoint } from './patient-endpoint.service';
import { MobUserLoginModel } from 'src/app/models/MobUserLoginModel.model';
import { map, mergeMap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Patient, PatientDoc } from 'src/app/models/patient/patient.model';
import { Doctor } from 'src/app/models/doctor/doctor.model';
import { Hospital } from 'src/app/models/hospital/hospital.model';
import { Visit } from 'src/app/models/visit/visit.model';


@Injectable()
export class PatientService {
  getStatus(VisitId?: string) {
    return this.patientEndPoint.getStatusEndpoint<Visit>(VisitId);
  } 
  constructor(private router: Router, private http: HttpClient,private patientEndPoint: PatientEndpoint) {
  }
  
  public _VisitId: string;
  get VisitId(): string
  {
    return this._VisitId;
  }
  set VisitId(VisitId: string) {
    this._VisitId = VisitId;
  }

  public _PatientId: string;
  get PatientId(): string {
    return this._PatientId;
  }
  set PatientId(PatientId: string) {
    this._PatientId = PatientId;
  }
  public _PatientByVisit: boolean;
  get PatientByVisit(): boolean {
    return this._PatientByVisit;
  }
  set PatientByVisit(PatientByVisit: boolean) {
    this._PatientByVisit = PatientByVisit;
  }

  getallbookinglist(PatientId?: string) {
    return this.patientEndPoint.getallbookinglistEndpoint<Patient[]>(PatientId);
  }
  getPatHistory(PatientId?: string) {
    return this.patientEndPoint.getPatHistoryEndpoint<Patient[]>(PatientId);
  } 
  getPatientinfo(PatientId?: string) {
    return this.patientEndPoint.getPatientByIdEndpoint<Patient>(PatientId);
  }
  
  getPatientHistory(PatientId?: string) {
    return this.patientEndPoint.getPatientHistoryEndpoint<Patient[]>(PatientId);
  } 

  updatePatient(patientinfo: Patient) {
    var patient = _.omit(patientinfo, ['PatValidator']);     
  
    if (patient.IdentityUserId) {
      return this.patientEndPoint.getUpdatePatientEndpoint(patient, patient.IdentityUserId);
    }
    else {
      return this.patientEndPoint.getUpdatePatientEndpoint<Patient>(patient.IdentityUserId).pipe(
        mergeMap(patientUser => {
          patient.IdentityUserId = patientUser.IdentityUserId;
          return this.patientEndPoint.getUpdatePatientEndpoint(patient, patient.IdentityUserId)
        }));
    }
  }
  bookappointment(visit: Visit, ScheduleIntervalId, SchedulingId) {
    var visit = _.omit(visit, ['PatientValidator']);
    return this.patientEndPoint.bookappointmentEndpoint<Visit>(visit, ScheduleIntervalId, SchedulingId);
  }
  getStatusByPatientUrl(PatientId?: string) {
    return this.patientEndPoint.getStatusByPatientUrlEndpoint<Visit>(PatientId);
  }
  getPatientByVisitId(VisitId?: string) {
    return this.patientEndPoint.getPatientByVisitIdEndpoint<Visit>(VisitId);
  }
  getPatientDocList(DoctorId?: string) {
    return this.patientEndPoint.getPatientDocListEndpoint<PatientDoc[]>(DoctorId);
  } 
  updateStatus(visit: Visit, VisitId) {
    return this.patientEndPoint.updateStatusEndpoint(visit, visit.VisitId);
  }
  getStatusByPatientId(PatientId?: string) {
    return this.patientEndPoint.getStatusByPatientIdEndpoint<Visit>(PatientId);
  }
  changeStatus(VisitId?: string) {
    return this.patientEndPoint.changeStatusEndpoint(VisitId);
  }
  updatePaidStatus(VisitId) {
    return this.patientEndPoint.updatePaidStatusEndpoint(VisitId);
  }
  getPaymentCharge(VisitId?: string) {
    return this.patientEndPoint.getPaymentChargeEndpoint<string>(VisitId);
  }

}
