import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginEndpoint } from './login-endpoint.service';
import { MobUserLoginModel } from 'src/app/models/MobUserLoginModel.model';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';


@Injectable()
export class LoginService {
  constructor(private router: Router, private http: HttpClient, private loginEndPoint: LoginEndpoint) {
  }

  GenerateOTP(mobileno) {
    return this.loginEndPoint.GenerateOTP(mobileno);
  }
 
  verifyOTP(sessionid,otp) {
    return this.loginEndPoint.verifyOTP(sessionid,otp);
  }
  // login with mob no and pass.
  loginMobnoandPass(lohinusr: MobUserLoginModel) {
      var omited = _.omit(lohinusr, ['UserValidator']);     
    return this.loginEndPoint.loginMobnoandPass(omited)
    .pipe(map(res => res));
  }
  // post mob no for login via otp
   postMobno(lohinusr: MobUserLoginModel) {
      var omited = _.omit(lohinusr, ['UserValidator']);     
    return this.loginEndPoint.postMobno(omited)
    .pipe(map(res => res));
  }
  // login via otp 
  LoginViaOTP(lohinusr: MobUserLoginModel) {
    var omited = _.omit(lohinusr, ['UserValidator']);     
  return this.loginEndPoint.LoginViaOTP(omited)
  .pipe(map(res => res));
}



}
