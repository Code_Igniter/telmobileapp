
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { EndpointFactory } from '../endpoint-factory.service';
import { catchError, map, share } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';


@Injectable()
export class LoginEndpoint extends EndpointFactory {
  public apiUrl = environment.api_Url;
 // private readonly _generateOTPUrl: string = "https://2factor.in/API/V1/793b129c-03a7-11eb-9fa5-0200cd936042/SMS/+91";  //@factor api frof otp genetare

  constructor(http: HttpClient, injector: Injector) {
    super(http, injector);
  }

  // GenerateOTP<T>(Mobno: string): Observable<T> {
   
  //   let endpointUrl = `${this._generateOTPUrl}+${Mobno}/${"AUTOGEN"}`;           
  //   return this.http.get<T>(endpointUrl).pipe(
  //     catchError(error => {
  //       return throwError(this.handleError(error, () => this.GenerateOTP(Mobno)));
  //     }));
  // }

  GenerateOTP<T>(Mobno: string): Observable<T> {
   
    return this.http.get<T>("https://2factor.in/API/V1/793b129c-03a7-11eb-9fa5-0200cd936042/SMS/+91"+Mobno+"/AUTOGEN").pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GenerateOTP(Mobno)));
      }));
  }
  verifyOTP<T>(session_id: string,otp: string): Observable<T> {
   
    return this.http.get<T>("https://2factor.in/API/V1/793b129c-03a7-11eb-9fa5-0200cd936042/SMS/VERIFY/" + session_id + "/" + otp).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.verifyOTP(session_id,otp)));
      }));
  }
  //login with mob and password
  loginMobnoandPass<T>(userObject: any): Observable<T> {

    return this.http.post<T>( this.apiUrl + `/api/account/login`, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      map(res => res), share(),
      catchError(error => {
        return throwError(this.handleError(error, () => this.loginMobnoandPass(userObject)));
      }));
  }  
    //post mob no
    postMobno<T>(userObject: any): Observable<T> {

      return this.http.post<T>( this.apiUrl + `/api/mobuserlogin/postmobno`, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
        map(res => res), share(),
        catchError(error => {
          return throwError(this.handleError(error, () => this.postMobno(userObject)));
        }));
    }    
     //Login Via OTP
     LoginViaOTP<T>(userObject: any): Observable<T> {

      return this.http.post<T>( this.apiUrl + `/api/mobuserlogin/loginviaotp`, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
        map(res => res), share(),
        catchError(error => {
          return throwError(this.handleError(error, () => this.LoginViaOTP(userObject)));
        }));
    }
}

// map((response: Response) => response.json()),
//           share()
