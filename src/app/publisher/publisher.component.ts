import { Component, ElementRef, AfterViewInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { OpentokService } from '../services/opentok.service';
import { PatientService } from '../services/patient/patient.service';


const publish = () => {

};

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})

export class PublisherComponent implements AfterViewInit {
  @ViewChild('publisherDiv') publisherDiv: ElementRef;
  @Input() session: OT.Session;
  publisher: OT.Publisher;
  publishing: Boolean;
  public VisitId : any;

  constructor(private opentokService: OpentokService, public patService: PatientService,public routing: Router,) {
    this.publishing = false;
    this.VisitId = this.patService.VisitId;
  }

  ngAfterViewInit() {
    const OT = this.opentokService.getOT();
    this.publisher = OT.initPublisher(this.publisherDiv.nativeElement, {insertMode: 'append'});
    this.opentokService.setpublisher(this.publisher);
    if (this.session) {
      if (this.session['isConnected']()) {      
        this.publish();
      }
      this.session.on('sessionConnected', () => this.publish());
      //this.session.on('streamDestroyed' ,() => this.unpublishstream());
    }
  }

  publish() {  
   
    this.session.publish(this.publisher, (err) => {
      if (err) {
        alert(err.message);
      } else {
        this.publishing = true;
      }
    });
  }



  cutcall(){ 
    this.patService.changeStatus(this.VisitId).subscribe(res => 
      this.SuccessChangestatus(res),
    res => this.Error(res));
    if(this.publisher){
      this.session.unpublish(this.publisher);
      if(this.session){
       //  this.session.disconnect();
      }
    }
 
    
    this.routing.navigate(["/pat_bookinglist"]);
   //this.session.disconnect();
   // this.opentokService.disconnect();
  }

  SuccessChangestatus(res){    
  }
  Error(res){
  }
 

}
