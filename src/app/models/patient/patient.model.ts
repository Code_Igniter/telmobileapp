export class Patient {

  public PatientId: string;
  public RegistrationNumber: string;
  public FirstName: string;
  public MiddleName: string;
  public LastName: string;
  public DateOfBirth: string;
  public MailingAddress: string;
  public Gender: string;
  public ContactNumber: string;
  public Address: string;
  public IsActive: boolean;
  public IdentityUserId: string;
  public CreatedDate:Date;

}
export class PatientDoc {
  PatientId: string;
  PatientName: string = "";
  Age: number;
  Sex: string = "";
  Email: string = "";
  VisitId: string;
  MobileNumber: string = "";
  Address: string = "";
  //prob: Array<Problem> = [];
  Fever: boolean = false;
  Cough: boolean = false;
  BreathingDifficulty: boolean = false;
  Tiredness: boolean = false;
  SoreThroat: boolean = false;
  Bodyache: boolean = false;
  ChestPain: boolean = false;
  IdentityUserId: string = "";
  Diarrhea: boolean = false;
  AnyOtherSymptoms: string = "";
  NMC: string = "";

  HeartDisease: boolean = false;
  HighBloodPressure: boolean = false;
  Diabetes: boolean = false;
  Copd: boolean = false;
  Transplant: boolean = false;
  RecentTravel: boolean = false;
  Cancer: boolean = false;
  Exposure: boolean = false;
  PatientMedicalSymptoms: string = "";
  OtherPertientInformation: string = "";
  Status: number = 0;
  Medication: string = "";
  TreatmentAdvice: string = "";
  FollowUp: string = "";
  LastUpdatedTime: Date;
}













