import { FormGroup, FormBuilder, Validators } from '@angular/forms';

export class MobUserLoginModel{
    public UserId: string;
    public PhoneNumber:string="";   
    public MobileNo:string="";   
    public Email:string="";
    public Gender:String="";
    public CreatedOn:string="";
    public Password: string = "";

    public UserValidator:FormGroup = null;

    constructor(){
        var _formBuilder = new FormBuilder();
        this.UserValidator = _formBuilder.group({
            // 'UserName': ['', Validators.compose([Validators.required, Validators.maxLength(30)])],
            // 'password': ['', Validators.compose([Validators.required])],          
            // 'Email': ['', Validators.pattern('^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}$')],
            "username": ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],

            // "phonenumber": ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
            "password": ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
 
        });

    }
    public IsDirty(fieldname): boolean {
        if (fieldname == undefined) {
            return this.UserValidator.dirty;
        }
        else {
            return this.UserValidator.controls[fieldname].dirty;
        }

    }

    public IsValid():boolean{
        if(this.UserValidator.valid)
        {
            return true;
        }
        else
        {
            return false;
        }
    } 

    public IsValidCheck(fieldname, validator): boolean {
        if (this.UserValidator.valid) {
            return true;
        }
        if (fieldname == undefined) {
            return this.UserValidator.valid;
        }
        else {

            return !(this.UserValidator.hasError(validator, fieldname));
        }
    }
    
    
  
}