

export class Doctor {
  public DoctorId: string;
  public UserName: string;
  public Password: string;
  public DoctorName:string;
  public FirstName: string;
  public MiddleName: string;
  public LastName: string;
  public NMC: string;
  public LongSignature: string;
  public DateOfBirth: string;
  public FilePath: string;
  public MailingAddress: string;
  public Gender: string;
  public PhoneNumber: string;
  public EnablePhNo: boolean;
  public Department: string;
  public HospitalId: string;
  public IsActive: boolean;
  public IdentityUserId: string;
  public ConfirmPassword: string;
  public DepartmentId: string;
  public HospitalDoctorMap: string;
  public Qualification : string; 
  public DoctorRoomName: string;
  public AdminId: string;
  public HospitalIdentifier: string;
}

// export class DoctorList {
//   public Designation: string;
//   public DoctorName: string;
//   public Education: string;
//   public Experience: string;
//   public FileName: string;
//   public FilePath: string;
//   public HospitalDoctorMapId: string;
//   public Membership: string;
//   public PastAffiliation: string;
//   public ScheduleList: Array<any> = new Array<any>();
// }
