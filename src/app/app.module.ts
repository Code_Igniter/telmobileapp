import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MessageService } from './services/message-service/message-service';
import { LoadingService } from './services/loading-service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginService } from './services/login/login.service';
import { LoginEndpoint } from './services/login/login-endpoint.service';
import { Utilities } from './services/utilities';
import { EndpointFactory } from './services/endpoint-factory.service';
import { ReactiveFormsModule } from "@angular/forms"
import { PatientEndpoint } from './services/patient/patient-endpoint.service';
import { PatientService } from './services/patient/patient.service';
 import { NetworkProvider } from './services/network-connection.service';
 import { Network } from '@ionic-native/network/ngx';
import { BookinglistPage } from './patient/allbookinglist/patient_bookinglist.page';
import { PatienthistoryPage } from './patient/pathistory/patient_history.page';
import { PatientprofilePage } from './patient/patprofile/patprofile.page';
import { HospitalService } from './services/hospital/hospital.service';
import { HospitalEndpoint } from './services/hospital/hospital-endpoint.service';
import { DoctorService } from './services/doctor/doctor.service';
import { DoctorEndpoint } from './services/doctor/doctor-endpoint.service';
import { doctorbookPage } from './doctor/doctorbook.page';
import { Global } from './app.global';
import { SchedulingService } from './services/scheduling/scheduling.service';
import { SchedulingEndpoint } from './services/scheduling/scheduling-endpoint.service';
import { DatePipe } from '@angular/common';

import { CalendarModule } from 'ion2-calendar';
import { APP_INITIALIZER } from '@angular/core';
import { ConfigService } from './services/appconfig.service';

import { SafePipe } from './services/pipe/selfepipe.pipe';
import { WaitingroomPage } from './Waitingroom/waitingroom.page';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { OpentokService } from './services/opentok.service';
import { SubscriberComponent } from './subscriber/subscriber.component';
import { PublisherComponent } from './publisher/publisher.component';
import { JitsiComponent } from './jitsi/jitsi.component';
import { TestAudioVideoComponent } from './jitsi/test-audio-video/test-audio-video.component';
import { HomePage } from './home/home.page';
import { LoginPage } from './login/login.page';


const initializerConfigFn = (config: ConfigService) => {
  return () => {
    var ret: any = config.loadAppConfig();
    return ret;
  };
};

@NgModule({
  declarations: [AppComponent,HomePage,LoginPage,WaitingroomPage,doctorbookPage,SubscriberComponent,PublisherComponent,SafePipe,JitsiComponent,TestAudioVideoComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,ReactiveFormsModule, CalendarModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, {
      provide: APP_INITIALIZER,
      useFactory: initializerConfigFn,
      multi: true,
      deps: [ConfigService],
    },
      //custom created services
      MessageService,
      LoadingService,    
      LoginService,
      LoginEndpoint,
      Utilities,
      EndpointFactory,
      PatientService,
      PatientEndpoint,
       NetworkProvider,
       Network,
       HospitalService,
       HospitalEndpoint,
       DoctorService,
       DoctorEndpoint,Global,SchedulingService,SchedulingEndpoint
       ,DatePipe,AndroidPermissions,Diagnostic,OpentokService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  
  constructor(g: Global) {
    // alert(g.config.videoUrl);
  }
}
