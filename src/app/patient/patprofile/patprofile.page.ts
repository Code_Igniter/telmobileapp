import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { PatientService } from '../../services/patient/patient.service';
import jwt_decode from 'jwt-decode';
import * as moment from 'moment-timezone';
import { MessageService } from 'src/app/services/message-service/message-service';
import { Patient } from 'src/app/models/patient/patient.model';
import { NgForm } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-home',
  templateUrl: './patprofile.page.html',
  styleUrls: ['./patprofile.page.scss'],
})
export class PatientprofilePage implements OnInit {

  ionicForm: FormGroup;

  isSubmitted = false;

  public folder: string;
  public token: any;
  public patInfo: Patient = new Patient();
  public editprofile: boolean = false;
  nepaliDate = moment();

  constructor(private activatedRoute: ActivatedRoute, public formBuilder: FormBuilder, public menueCtrl: MenuController, public patservice: PatientService,
    public messageService: MessageService, public authenticationService: AuthenticationService
  ) {
    this.token = this.authenticationService.currentUserValue;
    this.getPatientinfo();
  }
  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      patfirstname: ['', [Validators.required, Validators.minLength(2)]],
      patmiddelname: ['', [Validators.required, Validators.minLength(2)]],
      patlastname: ['', [Validators.required, Validators.minLength(2)]],
      patemail: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      // dob: [this.defaultDate],
      // mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }

  get errorControl() {
    return this.ionicForm.controls;
  }


  //enable side menue
  ionViewWillEnter() {
    this.menueCtrl.enable(true);
  }

  public getPatientinfo() {
    this.patservice.getPatientinfo(this.token.PatientIdentifier).subscribe(res => this.Success(res),
      res => this.Error(res));
  }
  Success(res) {
    this.patInfo = res;
  }
  Error(res) {
  }
  editPatprofile() {
    this.editprofile = true;
  }
  backToPatProfile(){
    this.editprofile=false;
  }


  public updatePatprofile() {
    this.isSubmitted = true;
    if (this.ionicForm.invalid) {
      this.messageService.errorMessage('Please provide all the required values!');

      return false;
    }
    else {
      const momentDate = new Date(this.patInfo.DateOfBirth);
      this.patInfo.DateOfBirth = moment(momentDate).format("YYYY/MM/DD");// converted gmtdate  to moment date
      this.patservice.updatePatient(this.patInfo).subscribe(res => this.updatepatSuccess(res),
        res => this.updatepatFailedError(res));
    }   
  }
  updatepatSuccess(res) {
    this.messageService.successMessage('Updated successful');
    this.editprofile = false;
  }
  updatepatFailedError(res) {
  }
}
