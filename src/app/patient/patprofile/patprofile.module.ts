import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PatientprofileRoutingModule } from './patprofile-routing.module';

import { PatientprofilePage } from './patprofile.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientprofileRoutingModule,
    ReactiveFormsModule
    // RouterModule.forChild([
    //   {
    //     path: '',
    //     component: HomePage
    //   }
    // ])
  ],
  declarations: [PatientprofilePage]
})
export class PatientprofileModule {}
