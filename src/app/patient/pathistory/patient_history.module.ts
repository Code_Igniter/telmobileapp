import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatienthistoryPageRoutingModule } from './patient_history-routing.module';

import { PatienthistoryPage } from './patient_history.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatienthistoryPageRoutingModule,
    // RouterModule.forChild([
    //   {
    //     path: '',
    //     component: HomePage
    //   }
    // ])
  ],
  declarations: [PatienthistoryPage]
})
export class PatienthistoryPageModule {}
