import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatienthistoryPage } from './patient_history.page';

const routes: Routes = [
  {
    path: '',
    component: PatienthistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatienthistoryPageRoutingModule {}
