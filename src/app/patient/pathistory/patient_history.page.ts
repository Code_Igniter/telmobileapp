import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { PatientService } from '../../services/patient/patient.service';
import jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-home',
  templateUrl: './patient_history.page.html',
  styleUrls: ['./patient_history.page.scss'],
})
export class PatienthistoryPage {
  public folder: string;
  public token: any;
  patList: any;
  collection = { count: 0, data: [] };

  constructor(private activatedRoute: ActivatedRoute, public menueCtrl : MenuController, public patservice : PatientService,
    public authenticationService: AuthenticationService
    ) {
          this.token = this.authenticationService.currentUserValue;
       this.AppointmentHistory();
   }
  //enable side menue
  ionViewWillEnter() {
    this.menueCtrl.enable(true);
     }

     public AppointmentHistory() {
      this.patservice.getPatHistory(this.token.PatientIdentifier). subscribe(res => this.Success(res),
      res => this.Error(res)); }
  
      Success(res){
        this.patList = res;
        this.collection = { count: this.patList.length, data: [] }
        for (var i = 0; i < this.collection.count; i++) {
          this.collection.data.push(
            {
              id: i + 1,
              VisitId: this.patList[i].VisitId,
              VisitDate: this.patList[i].VisitDate,
              DoctorName: this.patList[i].DoctorName,
              HospitalName: this.patList[i].HospitalName,
              Status: this.patList[i].Status,
            }
          );
        }
  
      }
      Error(res){
  
      }

     



}
