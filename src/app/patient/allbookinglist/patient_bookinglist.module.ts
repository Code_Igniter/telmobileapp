import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookinglistPageRoutingModule } from './patient_bookinglist-routing.module';

import { BookinglistPage } from './patient_bookinglist.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookinglistPageRoutingModule,
    // RouterModule.forChild([
    //   {
    //     path: '',
    //     component: HomePage
    //   }
    // ])
  ],
  declarations: [BookinglistPage]
})
export class BookinglistPageModule {}
