import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { BookinglistPage } from './patient_bookinglist.page';

describe('FolderPage', () => {
  let component: BookinglistPage;
  let fixture: ComponentFixture<BookinglistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookinglistPage ],
      imports: [IonicModule.forRoot(), RouterModule.forRoot([])]
    }).compileComponents();

    fixture = TestBed.createComponent(BookinglistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
