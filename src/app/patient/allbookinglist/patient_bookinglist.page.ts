import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { PatientService } from '../../services/patient/patient.service';
import jwt_decode from 'jwt-decode';
import * as moment from 'moment-timezone';
import { MessageService } from 'src/app/services/message-service/message-service';
import { Patient } from 'src/app/models/patient/patient.model';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
import { HospitalService } from 'src/app/services/hospital/hospital.service';
import { Router } from '@angular/router';
import { OpentokService } from 'src/app/services/opentok.service';
@Component({
  selector: 'app-home',
  templateUrl: './patient_bookinglist.page.html',
  styleUrls: ['./patient_bookinglist.page.scss'],
})
export class BookinglistPage {
  public folder: string;
  public token: any;
  patList: any;
  allbookinglist = { count: 0, data: [] };
  public paid : boolean;
  public paidbookingList : any;
  nearestVisit: Array<any> = new Array<any>();
  nepaliDate = moment();
  public showNearestVisit: boolean = false;
  public patInfo: Patient = new Patient();

  constructor(private activatedRoute: ActivatedRoute,  private opentokService: OpentokService,  public menueCtrl : MenuController, public patservice : PatientService,public routing: Router,
    public messageService: MessageService, public authenticationService: AuthenticationService , public docService :DoctorService, public hospitalService : HospitalService
    ) {
          this.token = this.authenticationService.currentUserValue;
          this.getPatientinfo();
          this.AppointmentHistory();
       this.showpaidlist();
     
   }

  
  //enable side menue
  ionViewWillEnter() {
    this.menueCtrl.enable(true);   
     }

     public getPatientinfo() {
      //var id = this.token.PatientIdentifier;
      this.patservice.getPatientinfo(this.token.PatientIdentifier).subscribe(
        (res) => {
          this.patInfo = res;
        },
        (error) => {
          this.messageService.errorMessage(error);
        }
      );
    }

     public AppointmentHistory() {
      this.patservice.getallbookinglist(this.token.PatientIdentifier). subscribe(res => this.Success(res),
      res => this.Error(res)); }
  
      Success(res){
        this.patList = res;
        this.allbookinglist = { count: this.patList.length, data: [] }
        for (var i = 0; i < this.allbookinglist.count; i++) {
          this.allbookinglist.data.push(
            {
              id: i + 1,
              VisitId: this.patList[i].VisitId,
              VisitDate: this.patList[i].VisitDate,
              DoctorName: this.patList[i].DoctorName,
              DepartmentName: this.patList[i].DepartmentName,
              PaymentStatus: this.patList[i].PaymentStatus,
              Status: this.patList[i].Status,
            }
          );
        }
  
      }
      Error(res){
  
      }
    
      showalllist(){
          this.paid = false;
          this.AppointmentHistory();
      }
      showpaidlist(){
          this.paid =true;          
            //this.patbook = true;
            this.patservice.getallbookinglist(this.token.PatientIdentifier).subscribe(res => {
              this.paidbookingList = res;
        
              for (var i = 0; i < this.paidbookingList.length; i++) {
                var currentNepaliDate = this.nepaliDate.tz("Asia/Kathmandu").format('YYYY/MM/DD HH:mm z');
                var currentNepaliDateTime = this.nepaliDate.tz("Asia/Kathmandu").format('YYYY/MM/DD HH:mm z');
                var currentdate = moment(currentNepaliDate, 'YYYY/MM/DD').format('YYYY/MM/DD');
                var currentDateWithTime = moment(currentNepaliDateTime, 'YYYY/MM/DD hh:mm').format('YYYY/MM/DD hh:mm');
                var currentYear = moment(currentNepaliDate, 'YYYY/MM/DD').format('YYYY');
                var currentMonth = moment(currentNepaliDate, 'YYYY/MM/DD').format('M');
                var currentDay = moment(currentNepaliDate, 'YYYY/MM/DD').format('D');
                this.paidbookingList[i].VisitDate = moment(this.paidbookingList[i].VisitDate).format('ll');
                if (this.paidbookingList[i].VisitDate == moment(this.nepaliDate).format('ll')) {
                  var todaydate = new Date();
                  var currentdate1 = this.nepaliDate.format('YYYY/MM/DD');
                  var index = this.paidbookingList[i].BookingTime.indexOf("-");
                  var timeextract = this.paidbookingList[i].BookingTime.substring(0, index - 1);
                  var bookedDateTime = moment(currentdate + ' ' + timeextract, 'YYYY/MM/DD HH:mm A');
                  var formatedBookedDateTime = moment(bookedDateTime, 'YYYY-MM-DD HH:mm A');
                  // var temp = moment(formatedBookedDateTime, 'YYYY-MM-DD HH:mm A').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm A');
                  // var formatedBookedDateTime = moment(temp, 'YYYY-MM-DDTHH:mm:s');
                  var DocStartTime = moment(currentdate + ' ' + this.paidbookingList[i].DocStartTime, 'YYYY/MM/DD HH:mm A');
                  var formatedreducedStartTime = moment(DocStartTime, 'YYYY-MM-DD HH:mm A');
                  var decStartTime = moment(formatedreducedStartTime, 'YYYY-MM-DD HH:mm A').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm A');
                  var formatedDecStartTime = moment(decStartTime, 'YYYY-MM-DDTHH:mm:A');
                  var DocEndtime = moment(currentdate + ' ' + this.paidbookingList[i].DocEndTime, 'YYYY/MM/DD HH:mm A');
                  var formattedEndTime = moment(DocEndtime, 'YYYY-MM-DD HH:mm A');
                  var currentTime1 = moment(todaydate).tz("Asia/Kathmandu").format('YYYY-MM-DD HH:mm z');
                  var currentTime = moment(currentTime1, 'YYYY/MM/DD hh:mm').format('YYYY-MM-DD HH:mm A');
                  var momentObj = moment(currentTime, 'YYYY-MM-DDLT');
                  var currentdateTime = momentObj.format('YYYY-MM-DDTHH:mm:s');
                  var formatedCurrentTime = moment(currentdateTime, 'YYYY-MM-DD HH:mm A');
                  //  var formatedCurrentTime = moment(currentTime, 'YYYY-MM-DD HH:mm A');
                  // var diffmin = formatedBookedDateTime.diff(formatedCurrentTime, 'minutes');
                  if (formatedDecStartTime <= formatedCurrentTime && formatedCurrentTime <= formattedEndTime) {
                    //if (diffmin <= 15) {
                    //  this.patBookList[i].GoToWaitingRoom = true;
                    //  this.patBookList[i].AccessPermission = true;
                    //  this.patBookList[i].EntryStatus = "go"
                    //} else {
                    //  this.patBookList[i].AccessPermission = false;
                    //  this.patBookList[i].EntryStatus = "todayupcoming";
                    this.paidbookingList[i].GoToWaitingRoom = true;
                    this.paidbookingList[i].AccessPermission = true;
                    this.paidbookingList[i].EntryStatus = "go";
                    this.nearestVisit = new Array<any>();
                    this.nearestVisit = JSON.parse(JSON.stringify(this.paidbookingList));//deepcopy
                    this.nearestVisit = this.paidbookingList[i];
                    this.showNearestVisit = true;
        
                  }
                  else {
                    var diffminwithStarttime = formatedCurrentTime.diff(formatedDecStartTime, 'minutes');
                    var diffminwithEndtime = formatedCurrentTime.diff(formattedEndTime, 'minutes');
                    if (diffminwithStarttime < 0) {
                      this.paidbookingList[i].AccessPermission = false;
                      this.paidbookingList[i].EntryStatus = "todayupcoming";
                    }
                    if (diffminwithEndtime > 0) {
                      this.paidbookingList[i].AccessPermission = false;
                      this.paidbookingList[i].EntryStatus = "todaypast";
                    }
                  }
        
        
                }
                if (this.paidbookingList[i].VisitDate < moment(currentDateWithTime).format('ll')) {
                  this.paidbookingList[i].AccessPermission = false;
                  this.paidbookingList[i].GoToWaitingRoom = false;
                  this.paidbookingList[i].EntryStatus = "missed";
                }
                if (this.paidbookingList[i].VisitDate > moment(currentDateWithTime).format('ll')) {
                  this.paidbookingList[i].AccessPermission = false;
                  this.paidbookingList[i].GoToWaitingRoom = false;
                  this.paidbookingList[i].EntryStatus = "upcoming";
                }
                this.paidbookingList[i].VisitDate = moment(this.paidbookingList[i].VisitDate).format('ll');
        
                this.paidbookingList[i].VisitStartTime = moment(this.paidbookingList[i].VisitStartTime).format('LT');
              }
              this.allbookinglist = { count: this.paidbookingList.length, data: [] }
        
              for (var i = 0; i < this.allbookinglist.count; i++) {
                this.allbookinglist.data.push(
                  {
                    id: i + 1,
                    VisitDate: this.paidbookingList[i].VisitDate,
                    HospitalName: this.paidbookingList[i].HospitalName,
                    DoctorName: this.paidbookingList[i].DoctorName,
                    DepartmentName: this.paidbookingList[i].DepartmentName,
                    VisitType: this.paidbookingList[i].VisitType,
                    Status: this.paidbookingList[i].Status,
                    ProviderId: this.paidbookingList[i].ProviderId,
                    HospitalId: this.paidbookingList[i].HospitalId,
                  }
                );
              }
            },
              error => {
                this.messageService.errorMessage('Something wrong');
                // this.loading = false;
              });
          
      }

      public GotoWaitingRoom(data){
        this.docService.DoctorId = data.ProviderId;
        this.hospitalService.HospitalId = data.HospitalId;    
        this.patservice.VisitId =data.VisitId;
        this.routing.navigate(["/waitingroom"]);
      }
     
pay(data){
  this.docService.DoctorId = data.ProviderId;
  this.hospitalService.HospitalId = data.HospitalId;    
  this.patservice.VisitId =data.VisitId;
  this.routing.navigate(["/payment"]);
}


}
