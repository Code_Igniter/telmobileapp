import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookAppointmentPage } from './book_appointment.page';


const routes: Routes = [
  {
    path: '',
    component: BookAppointmentPage,
  
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookAppointmentPageRoutingModule {}
