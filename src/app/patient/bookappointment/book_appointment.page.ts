import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { PatientService } from '../../services/patient/patient.service';


import { MessageService } from 'src/app/services/message-service/message-service';
import { Patient } from 'src/app/models/patient/patient.model';
import { HospitalService } from 'src/app/services/hospital/hospital.service';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
@Component({
  selector: 'app-home',
  templateUrl: './book_appointment.page.html',
  styleUrls: ['./book_appointment.page.scss'],
})
export class BookAppointmentPage {
  public token: any;   
  public doctor : boolean;
  public doctorlist : any;
  public hospitallist : any;  
  public patInfo: Patient = new Patient();
  public HospitalId: any;
  public doctordId: any;
  public showhospitalspecificdoclist : boolean = false;
  public hosdocmaplist : any;
  
  constructor(private activatedRoute: ActivatedRoute, public menueCtrl : MenuController, public patservice : PatientService,
    public messageService: MessageService, public authenticationService: AuthenticationService,
    public hospitalService: HospitalService,public docService: DoctorService ,public routing: Router,
    ) {
          this.token = this.authenticationService.currentUserValue;
       this.getuserdetail();
       this.getDoctorList();
      //  this.getHospitalList();
       
   }
  //enable side menue
  ionViewWillEnter() {
    this.menueCtrl.enable(true);
     }
     public getuserdetail() {
      this.patservice.getPatientinfo(this.token.PatientIdentifier). subscribe(res => this.getusrdetailSuccess(res),
      res => this.getusrdetaiFailedError(res)); }  
      getusrdetailSuccess(res){
        this.patInfo = res; 
      }
      getusrdetaiFailedError(res){
      }

     public getDoctorList() {       
      this.docService.getDoctorList(). subscribe(res => this.getdrlistSuccess(res),
      res => this.getdrlistFailedError(res)); }
      getdrlistSuccess(res){
        this.doctor = true;
        this.doctorlist = res;
      }
      getdrlistFailedError(res){
        }  

     public getHospitalList() {
      this.hospitalService.getHospitalList(). subscribe(res => this.getHospitalListSuccess(res),
      res => this.getHospitalListFailedError(res)); } 
      getHospitalListSuccess(res){
        this.doctor=false;
        this.hospitallist = res; 
      }
      getHospitalListFailedError(res){  
      }
  
      checkselected(event ){
        if ( event.detail.value =="doctor")
        {
          this.doctor = true;
                    this.getDoctorList()
        }
        else{
          this.doctor = false;
          this.getHospitalList();
        }
      }

      GoToHospital(id) {
        this.hospitalService.HospitalId = id;
        this.HospitalId = id;
        //this.routing.navigate(['/PatDashboard'], { queryParams: { id: this.hospitalService.HospitalId } });
    
        this.Gethospitalspecificdoclist();
        
      }
      GoToDoctor(id1) {
        this.showhospitalspecificdoclist =false;
        this.doctor =true;
        this.doctordId = id1;
        this.docService.DoctorId =this.doctordId
        this.routing.navigate(['doctorbook']);
       
      }

      Gethospitalspecificdoclist() {
        this.docService.getHospitalDoctorMapList(this.HospitalId)
          .subscribe(res => this.SuccessDoctor(res),
            res => this.Error(res));
      }
      SuccessDoctor(res) {
        if(res.lemgth < 0 ){
          this.messageService.waringMessage('Doctor not found');
        }
        this.hosdocmaplist = res;
        this.showhospitalspecificdoclist =true;
      }
      Error(res) {
    
      }

     



}
