export default {
  // Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
  // Do not include the trailing slash. See the README for more information:
  SAMPLE_SERVER_BASE_URL: 'http://YOUR-SERVER-URL',
  // OR, if you have not set up a web server that runs the learning-opentok-php code,
  // set these values to OpenTok API key, a valid session ID, and a token for the session.
  // For test purposes, you can obtain these from https://tokbox.com/account.
  API_KEY: '47054044',
  SESSION_ID: '2_MX40NzA1NDA0NH5-MTYwODUzMjMyNzgyMX5aSHBCR1hIaWFmRWtDTHdCZ2h3NVczR2h-fg',
  TOKEN: 'T1==cGFydG5lcl9pZD00NzA1NDA0NCZzaWc9MmRmNGExY2UzY2QxODYwMDJjMzIzODFlMjZmMTMzODdkNDY3NDJhOTpzZXNzaW9uX2lkPTJfTVg0ME56QTFOREEwTkg1LU1UWXdPRFV6TWpNeU56Z3lNWDVhU0hCQ1IxaElhV0ZtUld0RFRIZENaMmgzTlZjelIyaC1mZyZjcmVhdGVfdGltZT0xNjA4NTMyMzY2Jm5vbmNlPTAuODI2MTA4NTQ5NjE0NjE5NyZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNjExMzAwODcwJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9'
};
